#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()
ts1 = TouchSensor(Port.S1)
ts2 = TouchSensor(Port.S4)

# Constants
HEIGHT = 128
WIDTH = 178
BALL_SIZE = 5
DISTANCE_PLAYER_TO_EDGE = 10

# Write your program here.
def draw_middle_line():
    ev3.screen.draw_line(
        WIDTH /2,
        0,
        WIDTH /2,
        HEIGHT)

def draw_player_racket(x,y,color=Color.BLACK):
    width = 5
    height = 20
    if y < height:
        y = height
    elif y > HEIGHT:
        y = HEIGHT
    ev3.screen.draw_line(
        x,
        y - height,
        x, 
        y, 
        width,
        color
    )

def draw_player1(y,color=Color.BLACK):
    draw_player_racket(
        DISTANCE_PLAYER_TO_EDGE,
        y,
        color)

def draw_player2(y,color=Color.BLACK):
    draw_player_racket(
        WIDTH - DISTANCE_PLAYER_TO_EDGE,
        y,
        color)

def clear_player1(y):
    draw_player1(y,Color.WHITE)

def clear_player2(y):
    draw_player2(y,Color.WHITE)

def read_buttons():
    result = {}
    ev3_buttons = ev3.buttons.pressed()
    result["p1_up"] = Button.LEFT in ev3_buttons
    result["p2_up"] = Button.RIGHT in ev3_buttons
    result["p1_down"] = ts1.pressed()
    result["p2_down"] = ts2.pressed()
    return result

def update_player_positions(
    button_state,
    player1_position,
    player2_position
    ):
    if button_state["p1_up"]:
        # player1_position = player1_position - 1
        player1_position -= 1
    if button_state["p1_down"]:
        player1_position += 1
    if button_state["p2_up"]:
        player2_position -= 1
    if button_state["p2_down"]:
        player2_position += 1
    return (player1_position, player2_position)

def redraw_rackets(frame, player1_position, player2_position):
    button_state = read_buttons()
    new_player1_position = player1_position
    new_player2_position = player2_position
    if frame % 10 == 0:
        (new_player1_position, new_player2_position) = update_player_positions(button_state, player1_position, player2_position)
        if new_player1_position != player1_position:
            clear_player1(player1_position)
            draw_player1(new_player1_position)
        if new_player2_position != player2_position:
            clear_player2(player2_position)
            draw_player2(new_player2_position)
    return (new_player1_position, new_player2_position)


def draw_ball(x,y, color=Color.BLACK):
    ev3.screen.draw_line(x, y, x + BALL_SIZE, y, BALL_SIZE, color)

def clear_ball(x, y):
    draw_ball(x, y,Color.WHITE)

def update_ball_postion(x,y,direction):
    (x_new, y_new) = (x, y)
    x_new = x 
    if direction < 0:
        x_new = x - 1
    else:
        x_new = x + 1
    return (x_new, y_new)

def redraw_ball(x, y, frame, velocity):
    if frame % 10 == 0:
        clear_ball(x,y)
        (x_new, y_new) = update_ball_postion(x,y,1)
        draw_ball(x_new, y_new)
        return (x_new, y_new)
    else:
        return (x, y)
        

def main():
    player1_position = HEIGHT/2
    player2_position = HEIGHT/2
    frame = 0
    draw_player1(player1_position)
    draw_player2(player2_position)
    ball_direction = 1
    ball_x = WIDTH/2
    ball_y = HEIGHT/2
    draw_ball(ball_x,ball_y)
    while True:
        (player1_position, player2_position) = redraw_rackets(frame, player1_position, player2_position)
        draw_middle_line()
        (ball_x, ball_y) = redraw_ball(ball_x, ball_y, frame, 0)
        frame += 1
        frame = 0 if frame >= 60 else frame

# eine Änderung
main()